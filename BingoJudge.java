
public class BingoJudge {
	public static int Bingo(String[][] bingoCard) {
	int bingonum = 0;
	int rearchNum = 0;
	int holeCount = 0;

	// 左上から右下斜めにリーチがあるか
	for (int i = 0; i < bingoCard.length; i++) {
		if (bingoCard[i][i].contains("(")) {
			holeCount++;
		}
	}
	if (holeCount == 3) {
		rearchNum++;
	} else if (holeCount == 4) {
		bingonum++;
	}
	// 右上から左下斜めにリーチがあるか
	int holeCount1 = 0;
	int right = 4;
	for (int i = 0; i < bingoCard.length; i++) {
		if (bingoCard[i][right].contains("(")) {
			holeCount1++;
		}
		right--;
	}
	if (holeCount1 == 3) {
		rearchNum++;
	} else if (holeCount1 == 4) {
		bingonum++;
	}
	//横の選別
	for (int a = 0; a < 5; a++) {
		int holeCount2 = 0;
		for (int j = 0; j < 5; j++) {
			if (bingoCard[a][j].contains("(")) {
				holeCount2++;
			}
		}
		if (a == 2 && holeCount2 == 4) {
			bingonum++;
		} else if (a == 2 && holeCount2 == 3) {
			rearchNum++;
		} else if (holeCount2 == 5) {
			bingonum++;
		} else if (holeCount2 == 4) {
			rearchNum++;
		}
	}
	//縦の選別
	int diagonaleRarchNum4 = 0;
	int diagonalBingoNum4 = 0;
	for (int a = 0; a < 5; a++) {
		int holeCount3 = 0;
		for (int j = 0; j < 5; j++) {
			if (bingoCard[j][a].contains("(")) {
				holeCount3++;
			}
		}
		if (a == 2 && holeCount3 == 4) {
			bingonum++;
		} else if (a == 2 && holeCount3 == 3) {
			rearchNum++;
		} else if (holeCount3 == 5) {
			bingonum++;
		} else if (holeCount3 == 4) {
			rearchNum++;
		}

	}
	return bingonum;
	}
}
